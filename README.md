# Game

```
Document Index

1. Ages
    1.1 Stone Ages
        1.1.1 Buildings
    1.2 Bronze Ages
        1.2.1 Buildings
    1.3 Iron Age
        1.3.1 Buildings
2. Building Advance Scales
    2.1 Stone Age
        2.1.1 Sawmill
            2.1.1.1 Generations
            2.1.1.2 Levels
        2.1.2 Stone Mine
            2.1.2.1 Generations
            2.1.2.2 Levels
    2.2 Bronze Age
        2.2.1 Sawmill
            2.2.1.1 Generations
            2.2.1.2 Levels
    2.3 Iron Age
        2.3.1 Sawmill
            2.3.1.1 Generations
            2.3.1.2 Levels
3. Contributing
    3.1 Developing Code
    3.2 Edit This Document
```

## 1. Ages

### 1.1 Stone Age

#### 1.1.1 Buildings

| Name             | Required | Advance table |
| ---------------- | -------- | ------------- |
| Sawmill          | -        | [§ 2.1.1 Sawmill](####2.1.1-Sawmill) |
| Stone Mine       | Sawmill  | [§ 2.1.2 Stone Mine](####2.1.2-Stone-Mine) |

### 1.2 Bronze Age

#### 1.2.1 Buildings

| Name             | Required | Advance table |
| ---------------- | -------- | ------------- |
| Sawmill          | -        | [§ 2.2.1 Sawmill](#2.2.1-Sawmill) |

### 1.3 Iron Age

#### 1.3.1 Buildings

| Name             | Required | Advance table |
| ---------------- | -------- | ------------- |
| Sawmill          | -        | [§ 2.3.1 Sawmill](#2.3.1-Sawmill) |


## 2 Building Advance Scales

### 2.1 Stone Age

#### 2.1.1 Sawmill

##### 2.1.1.1 Generations

| Generation | Description |
| :---: | :---: |
| 1 | Basic Wooden Sawmill |
| 2 | Stone Sawmill Building |
| 3 | Advanced Sawmill Building ([Bronze age](#1.2.1-Buildings)) |

##### 2.1.1.2 Levels

| Generation | Level | Units # | Advance required | Advance cost | Generates/minute | Costs |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| 1 | 1 | 2 | - | 20x wood | 5 wood | - |
| 1 | 2 | 2 | - | 30x wood | 6 wood | - |
| 1 | 3 | 3 | - | 40x wood | 7 wood | - |
| 1 | 4 | 3 | - | 50x wood | 8 wood | - |
| 2 | 5 | 5 | Stone Mine @ level 3 | 50x wood, 100x stone | 10 wood | - |
| 2 | 6 | 5 | - | 75x wood, 125x stone | 12 wood | - |
| 2 | 7 | 5 | - | 80x wood, 125x stone | 13 wood | - |
| 2 | 8 | 5 | - | 100x wood, 150x stone | 16 wood | - |
| 2 | 9 | 5 | - | 100x wood, 200x stone | 19x wood | - |
| 2 | 10 | 5 | Stone Mine @ level 7 | 125x wood, 200x stone | 22x wood | - |
| 2 | 11 | 5 | - | 150x wood, 200x stone | 24x wood | - |
| 2 | 12 | 5 | - | 150x wood, 250x stone | 26x wood | - |
| 2 | 13 | 5 | - | 400x wood, 300x stone | 28x wood | - |
| 2 | 14 | 5 | - | 800x wood, 300x stone | 30x wood | - |
| [3, Bronze Age](#####2.1.1.1-Generations) | 15 | 4 | Bronze Age / Bronze Mine @ Level 10 | 1250x wood, 1000x stone, 50x bronze | 35x wood | - |

#### 2.1.2 Stone Mine

##### 2.1.2.1 Generations

| Generation | Description |
| :---: | :---: |
| 1 | Stone Mine |
| 2 | Advanced Stone Mine |
| 3 | Bronze Mine ([Bronze age](#1.2.1-Buildings)) |

##### 2.1.2.2 Levels

| Generation | Level | Units # | Advance required | Advance cost | Generates/minute | Costs |
| --- | --- | --- | --- | --- |
| 1 | 1 | 6 | - |10x wood, 20x stone | 3 stone | - |

### 2.2 Bronze Age

#### 2.2.1 Sawmill

##### 2.2.1.1 Generations

| Generation | Description |
| :---: | :---: |
| 3 | Advanced Sawmill Building |
| 4 | Sawmill Factory ([Iron Age](####1.3.1-Buildings)) |

##### 2.2.1.2 Levels

| Generation | Level | Units # | Advance required | Advance cost | Generates/minute | Costs |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| 3 | 15 | 4 | Bronze Age / Bronze Mine @ Level 10 | 1.250x wood, 1.000x stone, 50x bronze | 35x wood | - |
| 3 | 16 | 4 | - | 1.500x wood, 1.000x stone, 200x bronze | 42x wood | - |
| 3 | 17 | 4 | Stone Mine @ Level 15 | 4.000x wood, 3.000x stone, 500x bronze | 51x wood | - |
| 3 | 18 | 4 | - | 5.000x wood, 2.500x stone, 750x bronze | 59x wood | - |
| 3 | 19 | 4 | - | 6.125x wood, 3.000x stone, 1.000x bronze | 69x wood | - |
| 4 | 20 | 5 | Iron Age / Iron Mine @ Level 15 | 3.000x stone, 1.000x bronze, 500x iron | 88x wood | - |

### 2.3 Iron Age

#### 2.3.1 Sawmill

##### 2.3.1.1 Generations

| Generation | Description |
| :---: | :---: |
| 4 | Sawmill Factory |
| 5 | Advanced Sawmill Factory |
| 6 | Ultimate Sawmill Factory |

##### 2.3.1.2 Levels

| Generation | Level | Units # | Advance required | Advance cost | Generates/minute | Costs |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| 4 | 20 | 5 | Iron Age / Iron Mine @ Level 15 | 3.000x stone, 1.000x bronze, 500x iron | 88x wood | 1 iron/5 minutes |
| 4 | 21 | 6 | - | 1.000x iron | 104x wood | 1 iron/5 minutes |
| 4 | 22 | 6 | - | 2.000x iron | 183x wood | 1 iron/4 minutes |
| 4 | 23 | 6 | - | 2.500x iron | 251x wood | 1 iron/4 minutes |
| 4 | 24 | 6 | - | 3.000x iron | 320x wood | 1 iron/4 minutes |
| 5 | 25 | 8 | Iron Mine @ Level 20 | 30.000x wood, 25.000x stone, 5.000x bronze, 10.000x iron | 800x wood | 1 iron/minute |
| 6 | 26 | 12 | Gold Mine @ Level 25 | 100.000x wood, 75.000x stone, 30.000x bronze, 75.000 iron, 10.000x gold | 2.000x wood | 1 bronze/minute, 2 iron/minute

## 3 Contributing

### 3.1 Developing Code

To help contributing create a [bitbucket.org](https://bitbucket.org/) account, download [SourceTree](https://www.sourcetreeapp.com/), and clone the project. You can now work on the code and when you're done create a pull request. All development work will be based on master, the releases will be tagged and feature/bug/hotfix branches can be created if needed.

### 3.2 Edit This Document

Go to this document in the source view ([Direct Link](https://bitbucket.org/WouterG/mmorpg-game/src/HEAD/README.md?at=master&fileviewer=file-view-default)) and click Edit in the top-left corner. The document uses the [Markdown Syntax](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet).
